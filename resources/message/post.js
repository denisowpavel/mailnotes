var d = new Date();
this.date = d.toString();
this.dateUnix = (d.getTime() / 1000).toFixed(0);

dpd.email.post({
 to      : this.to,
 subject : this.from,
 text    : this.message
}, function ( err, results ) {
   this.mailResult = {err:err, results:results};
});