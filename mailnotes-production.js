// production.js
var deployd = require('deployd');

var server = deployd({
  port: 5006,
  env: 'production',
  db: {
    host: 'localhost',
    port: 27017,
    name: 'mailnotes',
    credentials: {
      username: 'mailnotesdbuser',
      password: '*********'
    }
  }
});

server.listen();

server.on('listening', function() {
  console.log("Server is listening");
});

server.on('error', function(err) {
  console.error(err);
  process.nextTick(function() {
    process.exit();
  });
});